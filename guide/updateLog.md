### 更新指南

Meri Plus 遵循 [Semver](https://semver.org/lang/zh-CN/) 语义化版本规范。 

### 发布节奏

- 修订版本号：每周末会进行日常 bugfix 更新。（如果有紧急的 bugfix，则任何时候都可发布）
- 次版本号：每月发布一个带有新特性的向下兼容的版本。
- 主版本号：含有破坏性更新和新特性，不在发布周期内。 

### 更新日志

#### 1.0.0

- 🌟 正式发版了

- 0.1.102: Table 新增 activeRows 属性，完成表格多行高亮显示
- 0.1.103:
 - Table:复选框表头触发区域修改为整个单元格
 - Radio:
  - 样式修改：单选框选中原点尺寸放大的比例修改为50%，diabled 状态下未选中单选框圆圈背景颜色修改为gray-0
 - UploadFile & UploadImage : 
  -  preview:上传组件的新增预览属性preview
  - Bug修复:修改初始化默认值不显示默认图片的Bug
  - 样式修改:修改报错文字的间距以及字体大小
- 0.1.104:
 - 修复编译：import require$$0 from 'vue' 导致的问题
- 0.1.105:
 - UploadFile & UploadImage:
  - 修改在delete 回调函数参数中会将传入的自定义属性添加
  - 修改预览问题的层级，自动增长

