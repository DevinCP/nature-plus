/*
 * @Author: Devin
 * @Date: 2022-06-15 11:17:07
 * @LastEditors: chenpeng 1983029606@qq.com
 * @LastEditTime: 2023-04-06 20:28:01
 */
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";

export default defineConfig({
  base: "/",
  plugins: [vue(), vueJsx()],
  esbuild: {
    jsxFactory: "h",
    jsxFragment: "Fragment",
  },
  server: {
    port: 3011,
    proxy:{
      "/api":"https://gapmdev.persagy.com"
    }
  },
});
