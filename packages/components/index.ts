import 'core-js/actual';
import { App, Plugin } from 'vue';
export * from './components';
import * as components from './components';
import 'vite/modulepreload-polyfill';
import '../theme/theme.css';
import 'meri-icon/lib/style.css';
const install = (app: App, prefix: string = 'N') => {
    Object.values(components).forEach(comp => app.use(comp as Plugin, prefix));
};

export default install;
